﻿using System;
using System.Threading;
using LearningNinject.Abstractions;
using LearningNinject.Encryptors;
using LearningNinject.IoC;
using LearningNinject.Warrior;
using Ninject;
using Xunit;

namespace LearningNinject.Tests
{
    public class DomainContainerTests
    {

        [Fact]
        public void Should_InjectCorrectImplemenation_When_CreatingAnObject()
        {
            Samurai samurai;

            using (var kernelSut = new StandardKernel(new WarriorModule()))
            {
                samurai = kernelSut.Get<Samurai>();
            }


            var expected = WeaponType.Sword;
            var actual = samurai.Weapon.WeaponType;



            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_InjectCorrectImplementation_When_CreatingAnotherObject()
        {
            Rifleman rifleman;



            using (var kernelSut = new StandardKernel(new WarriorModule()))
            {
                rifleman = kernelSut.Get<Rifleman>();
            }




            var expected = WeaponType.Rifle;
            var actual = rifleman.Weapon.WeaponType;




            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_ReturnTheSameInstance_When_BindSettingIsSingletonScope()
        {
            IClasses expected;
            IClasses actual;



            using (var kernelSut = new StandardKernel())
            {
                kernelSut.Bind<IClasses>().To<Classes>().InSingletonScope();

                expected = kernelSut.Get<IClasses>();
                actual = kernelSut.Get<IClasses>();
            }



            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Should_ReturnTheSameInstance_When_BindSettingSetToThreadScope()
        {
            IClasses expected;
            IClasses actual;



            using (var kernelSut = new StandardKernel())
            {
                kernelSut.Bind<IClasses>().To<Classes>().InThreadScope();

                expected = kernelSut.Get<IClasses>();
                actual = kernelSut.Get<IClasses>();
            }



            Assert.Same(expected, actual);
        }



        /*If we replace Assert.NotSame() with Assert.Same(), we'll first get the impression that the test pass.
         * Run the test in debug-mode and the we will that test fail.
        */
        [Fact]
        public void Should_ReturnADifferentInstance_When_BindSettingSetToThreadScopeAndObjectsAreOnDifferentThreads()
        {
            var kernelSut = new StandardKernel();
            kernelSut.Bind<IClasses>().To<Classes>().InThreadScope();

            var objectOnThreadOne = kernelSut.Get<IClasses>();




            new Thread(() =>
            {
                var objectOnThreadTwo = kernelSut.Get<IClasses>();



                Assert.NotSame(objectOnThreadOne, objectOnThreadTwo);

                kernelSut.Dispose();

            }).Start();

        }

        [Fact]
        public void Should_ReturnTheSameInstance_When_BindSettingSetToCustomScopeAndObjectStateHasNotChanged()
        {
            User userOne;
            User userTwo;



            using (var kernelSut = new StandardKernel())
            {
                kernelSut.Bind<User>().ToSelf().InScope(ctx => User.Current);


                User.Current = new User();
                userOne = kernelSut.Get<User>();

                userTwo = kernelSut.Get<User>();




                Assert.Same(userOne, userTwo);
            }
        }

        [Fact]
        public void Should_ReturnADifferentInstance_When_BindSettingSetToCustomScopeAndObjectStateHasChanged()
        {
            User userOne;
            User userTwo;



            using (var kernelSut = new StandardKernel())
            {
                kernelSut.Bind<User>().ToSelf().InScope(ctx => User.Current);


                User.Current = new User();
                userOne = kernelSut.Get<User>();

                User.Current = new User();
                userTwo = kernelSut.Get<User>();




                Assert.NotSame(userOne, userTwo);
            }
        }


        [Fact]
        public void Should_ReturnExpectedType_When_XMLConfigHasLoaded()
        {
            var expectedType = typeof(ShiftEncryptor);
            Type resultType;

            using (var kernelSut = new StandardKernel())
            {


                kernelSut.Load("TypeRegistrations.xml");

                var r = kernelSut.Get<IEncryptor>();
                resultType = r.GetType();
            }



            Assert.Equal(expectedType, resultType);
        }
        

    }
}
