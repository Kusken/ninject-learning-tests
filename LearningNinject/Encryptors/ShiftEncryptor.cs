﻿using System.Linq;
using LearningNinject.Abstractions;

namespace LearningNinject.Encryptors
{
    public class ShiftEncryptor : IEncryptor
    {
        public string EncryptString(string str)
        {
            return IncrementUnicodeCharsByOne(str);
        }

        private static string IncrementUnicodeCharsByOne(string str)
        {
            return str.Select(character => (char)(character + 1)).ToString();
        }
    }
}