﻿using System.Linq;
using LearningNinject.Abstractions;

namespace LearningNinject.Encryptors
{
    public class ReverseEncryptor : IEncryptor
    {
        public string EncryptString(string str)
        {
            return str.Reverse().ToString();
        }
    }
}