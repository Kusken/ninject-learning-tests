﻿namespace LearningNinject.Abstractions
{
    public abstract class Warrior
    {
        private IWeapon weapon;


        protected Warrior(IWeapon weapon)
        {
            this.weapon = weapon;
        }


        public IWeapon Weapon => weapon; 


        public abstract void Attack();
    }
}