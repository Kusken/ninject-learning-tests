﻿namespace LearningNinject.Abstractions
{
    public interface IEncryptor
    {
        string EncryptString(string str);
    }
}