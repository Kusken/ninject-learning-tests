﻿namespace LearningNinject.Abstractions
{
    public interface IWeapon
    {
        WeaponType WeaponType { get; }
        float Damage { get; }
    }
}