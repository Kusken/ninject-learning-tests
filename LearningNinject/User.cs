﻿namespace LearningNinject
{
    public class User
    {
        public string Name { get; set; }
        public static User Current { get; set; }
    }
}