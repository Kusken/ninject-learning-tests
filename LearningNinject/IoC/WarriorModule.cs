﻿using LearningNinject.Abstractions;
using LearningNinject.Warrior;
using Ninject.Modules;

namespace LearningNinject.IoC
{
    public class WarriorModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IWeapon>().To<Katana>().WhenInjectedInto<Samurai>();
            Bind<IWeapon>().To<Rifle>().WhenInjectedInto<Rifleman>();
        }
    }
}