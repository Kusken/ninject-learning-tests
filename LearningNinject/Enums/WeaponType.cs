﻿namespace LearningNinject
{
    public enum WeaponType
    {
        Sword,
        Bow,
        Rifle,
        Gun,
        Dagger,
        Pike,
        Spear,
        Mace,
        Warhammer
    }
}