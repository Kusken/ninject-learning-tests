﻿using System;
using LearningNinject.Abstractions;

namespace LearningNinject.Warrior
{
    public class Samurai : Abstractions.Warrior
    {
        public Samurai(IWeapon weapon) : base(weapon)
        {
        }

        public override void Attack()
        {
            Console.WriteLine($"The Samurai swings his {Weapon}");
        }
    }
}