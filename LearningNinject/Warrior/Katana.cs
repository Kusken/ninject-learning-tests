﻿using LearningNinject.Abstractions;

namespace LearningNinject.Warrior
{
    public class Katana : IWeapon
    {
        private float damage = 3.2f;


        public Katana()
        {
            WeaponType = WeaponType.Sword;
        }


        public WeaponType WeaponType { get; }
        public float Damage => damage;
    }
}
