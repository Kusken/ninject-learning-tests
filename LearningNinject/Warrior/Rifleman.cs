﻿using System;
using LearningNinject.Abstractions;

namespace LearningNinject.Warrior
{
    public class Rifleman : Abstractions.Warrior
    {
        public Rifleman(IWeapon weapon) : base(weapon)
        {
        }


        public override void Attack()
        {
            Console.Write($"The Rifleman fires his {Weapon}");
        }

    }
}
