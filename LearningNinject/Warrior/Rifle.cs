﻿using LearningNinject.Abstractions;

namespace LearningNinject.Warrior
{
    public class Rifle : IWeapon
    {
        private float damage = 15.2f;


        public Rifle()
        {
            WeaponType = WeaponType.Rifle;;
        }


        public WeaponType WeaponType { get; }
        public float Damage => damage;
    }
}
