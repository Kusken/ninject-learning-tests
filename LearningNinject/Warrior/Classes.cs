﻿using System.Collections.Generic;
using LearningNinject.Abstractions;
using LearningNinject.IoC;
using Ninject;

namespace LearningNinject.Warrior
{
    public class Classes : IClasses
    {
        private List<Abstractions.Warrior> warriors;

        public Classes()
        {
            using (var kernel = new StandardKernel(new WarriorModule()))
            {
                warriors = new List<Abstractions.Warrior>
                {
                    kernel.Get<Samurai>(),
                    kernel.Get<Rifleman>()
                };
            }
        }


        public IEnumerable<Abstractions.Warrior> Warriros => warriors;
    }

}
